# README #

This README document provides the needed instructions to get the application up and running.


### Installation ###

* Clone the repository [beer-pro](https://bitbucket.org/khaledirk/beer-pro/src/master/) into your local driver
* Open a terminal and type 
> npm i
* Run the application using the following command (Application will start on port 1337)
> npm start
* Run the test using command
> npm test

### Endpoints ###

Method | Request Headers | Endpoint | Request Body
------ | ------- | ---------| ------------
GET | x-user: email | api/beers?name=string | N/A
POST | content-type: application/json, x-user: email | api/beers/int | { "rating": int, "commnets": string }

### Databases Location ###

* Beer Rating ./server/data/beersDb.nosql
* User Requests ./server/logs/logsDb.nosql
