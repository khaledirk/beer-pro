const path = require('path');
const { getLogger } = require('./server/lib/logger');
const ExpressService = require('./server');

process.env.NODE_CONFIG_DIR = path.join(__dirname, './server/config');
global.config = require('config');
// use winston logger for coloring and format features
global.log = getLogger(config.log, config.timezone);

process.env.NODE_ENV = (process.env.NODE_ENV || 'development');
const EXPRESS_PORT = (config.port || 1337);

let server = null;
let stopped = false;
// shutdown the server if CTRIL + C or if exception occured
async function gracefulShutdown() {
  if (!stopped) {
    stopped = true;
    log.info('Performing shutdown. Please allow at least 30 minutes for any in progress tasks to '
      + 'complete before manule termintation');
    if (server) {
      log.info('Starting shutdown Express server');
      await server.stop();
    } else {
      log.info('Shutdown already in progress');
    }
  }
}
// CTRL + C
process.on('SIGINT', gracefulShutdown);
// Error
process.on('SIGTERM', gracefulShutdown);

//starting up the express server
const bootstrap = async () => {
  try {
    log.info('Application is starting...');
    server = new ExpressService(EXPRESS_PORT);
    server.start();
    log.info(`Express server listening on port ${EXPRESS_PORT}`);
  } catch (err) {
    console.log(err);
    log.error('Application Error', { message: err.message });
  }
}

bootstrap();

module.exports = server;
