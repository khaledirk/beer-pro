const request = require('supertest');
const server = require('../app');

describe('Test x-user header check', () => {
  const { app, start, stop } = server;

  it('should reponse with 400 with No x-user GET', async () => {
    const res = await request(app).get('/api/beers?name=Berliner');
    expect(res.statusCode).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.title).toBe('Missing or invalid x-user email value in the request header');
  });
  it('should reponse with 400 with No x-user POST', async () => {
    const res = await request(app).post('/api/beers/2');
    expect(res.statusCode).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.title).toBe('Missing or invalid x-user email value in the request header');
  });
  it('should reponse with 400 with invalid x-user GET', async () => {
    const res = await request(app).get('/api/beers?name=Berliner').set('x-user', 'username@domain');
    expect(res.statusCode).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.title).toBe('Missing or invalid x-user email value in the request header');
  });
  it('should reponse with 400 with invalid x-user POST', async () => {
    const res = await request(app).post('/api/beers/2').set('x-user', 'usernamedomain.com');
    expect(res.statusCode).toBe(400);
    expect(res.body.code).toBe(400);
    expect(res.body.title).toBe('Missing or invalid x-user email value in the request header');
  });
  it('should reponse with 200 with valid x-user GET', async () => {
    const res = await request(app).get('/api/beers?name=Berliner').set('x-user', 'username@domain.com');
    expect(res.statusCode).toBe(200);
  });
  it('should reponse with 200 with valid x-user POST', async () => {
    const res = await (await request(app).post('/api/beers/2').set('x-user', 'username@domain.com')
      .send({ rating: 5, comments: 'unit testing x-user header' }));
    expect(res.statusCode).toBe(200);
  });
});
