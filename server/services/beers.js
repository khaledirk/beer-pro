import _ from 'lodash';
import {
  findBeerRatingById,
  insertBeerRating,
  updateBeerRating,
} from '../lib/db_operations';
import { findBeersByName, isBeerIdExists } from '../lib/punk_operations';
import { Promise } from 'bluebird';
import { UserException } from '../lib/exceptions';

export async function getBeersByName(name) {
  // As query string name is required, validate it exist and not empty
  // otherwise return message to user
  if (_.isNil(name) || _.isEmpty(name)) {
    throw new UserException('Query string \'name\' is required');
  }
  
  const punkResults = await findBeersByName(name);

  // Bind the rating and comments from NoSQL with the filtered results
  return Promise.all(_.map(punkResults, async result => {
    const { id, name, description, first_brewed, food_pairing } = result;
    const dbResults = await findBeerRatingById(id);
    let rating, comments;
    if (!_.isEmpty(dbResults)) {
      rating = dbResults[0].rating;
      comments = dbResults[0].comments;
    }
    return ({
      id, name, description, first_brewed, food_pairing, rating, comments
    })
  }));
}

export async function setRating(_id, data) {
  // Validate paramter id exist and is number
  if (_.isEmpty(_id) || !Number.isInteger(_.toNumber(_id))) {
    throw new UserException('Invalid parameter id!');
  }
  const id = _.toNumber(_id);
  // Valid given beer id exist in Punk API
  if(!await isBeerIdExists(id)){
    throw new UserException(`Beer ID ${id} doesn't exist`);
  }
  const { rating, comments } = data;
  // Validate rating to fall in the range of (1-5)
  if (_.isNil(rating) || !Number.isInteger(rating) || !_.inRange(rating, 1, 6)) {
    throw new UserException('Invalid Rating format! Accpetable values (1-5)');
  }

  const results = await findBeerRatingById(id);
  if (_.isEmpty(results)) {
    insertBeerRating(id, rating, comments);
  } else {
    updateBeerRating(id, rating, comments);
  }
}
