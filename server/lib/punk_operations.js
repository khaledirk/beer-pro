import axios from 'axios';
import cache from 'memory-cache';
import crypto from 'crypto';
import _ from 'lodash';

export async function findBeersByName(name) {
  const { punk } = config;
  const request = {
    method: 'get',
    url: `https://${punk.ip}/${punk.version}/${punk.endpoint}?${punk.queryName}=${name}`
  }
  // Hash the request before storing it as key in the cache
  const cacheKey = crypto.createHash('md5').update(request.method + request.url).digest('hex');
  // If results already cached then return. Otherwise cache the new returns for one minutes
  if (_.includes(cache.keys(), cacheKey)) {
    return cache.get(cacheKey);
  }

  const results = (await axios(request)).data;
  cache.put(cacheKey, results, punk.cacheTime);
  return results;
}

// Lookup the given beer id if it exists in the Punk
export async function isBeerIdExists(id){
  const { punk } = config;
  const request = {
    method: 'get',
    url: `https://${punk.ip}/${punk.version}/${punk.endpoint}/${id}`
  }
  const cacheKey = crypto.createHash('md5').update(request.method + request.url).digest('hex');
  if (_.includes(cache.keys(), cacheKey)) {
    return cache.get(cacheKey);
  }
  try {
    await axios(request);
    cache.put(cacheKey, true, punk.cacheTime);
    return true;
  } catch (err) {
    cache.put(cacheKey, false, punk.cacheTime);
    return false;
  }
}
