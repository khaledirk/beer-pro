import HttpStatus from 'http-status-codes';

export function UserException(message) {
  this.code = HttpStatus.BAD_REQUEST;
  this.title = message;
}
