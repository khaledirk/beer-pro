import NoSQL from 'nosql';
import path from 'path';
import moment from 'moment-timezone';
import { Promise } from 'bluebird';

const beersDb = NoSQL.load(path.join(__dirname, '../data/beersDb.nosql'));
const logsDb = NoSQL.load(path.join(__dirname, '../logs/logsDb.nosql'));

export function findBeerRatingById(id) {
  return new Promise((resolve, reject) => {
    beersDb.find().make(builder => {
      builder.where('id', id);
      builder.callback((err, response) => {
        if (!err) {
          resolve(response);
        } else {
          reject(err);
        }
      });
    });
  });
}

export function insertBeerRating(id, rating, comments) {
  beersDb.insert({ id, rating, comments });
}

export function updateBeerRating(id, rating, comments) {
  beersDb.modify({ rating, comments }).where('id', id);
}

export function insertLog(user, method, url, body) {
  const timestamp = new moment().tz(config.timezone).format()
  logsDb.insert({ timestamp, user, method, url, body });
}
