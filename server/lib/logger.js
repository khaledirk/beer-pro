import moment from 'moment-timezone';
import winston from 'winston';

export function getLogger(log, timezone){
  // customize the log color during the initialization of winston logger
  const logger = new (winston.Logger)({
    levels: {
      silent: 0,
      error: 1,
      warn: 2,
      debug: 3,
      info: 4,
      verbose: 5,
      silly: 6,
    },
    colors: {
      silent: 'white',
      error: 'red',
      warn: 'yellow',
      debug: 'blue',
      info: 'green',
      verboose: 'cyan',
      silly: 'grey',
    },
  });

  // log given information to the main console
  // use log level and timezone that are specified in the config file
  logger.add(winston.transports.Console, {
    level: log.level,
    handleExceptions: true,
    json: false,
    colorize: true,
    timestamp: () => {
      const time = new moment();
      return time.tz(timezone).format();
    },
  });
 
  return logger;
}
