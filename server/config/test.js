module.exports = {
  port: 1336,
  log: {
    level: 'verbose'
  },
  timezone: 'America/Edmonton',
  punk: {
    ip: 'api.punkapi.com',
    version: 'v2',
    endpoint: 'beers',
    queryName: 'beer_name',
    cacheTime: 60000  // one minute
  }
};
