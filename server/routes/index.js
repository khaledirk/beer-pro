import express from 'express';
import beers from './beers';

const routes = express.Router();
// all related to endpoint beers will be in route beers 
routes.use('/beers', beers);

export default routes;
