import express from 'express';
import HttpStatus from 'http-status-codes';
import _ from 'lodash';
import { getBeersByName, setRating } from '../services';

const route = express();

route.get('/', async (req, res) => {
  try {
    const beerName = req.query.name;
    // handle the validation and the logic in services
    const response = await getBeersByName(beerName);
    res.send(response);
  } catch (err) {
    // if any error occur in db or REST call, this will catch here 
    // and user will be send the error
    log.error(err);
    res.status(HttpStatus.BAD_REQUEST).send(err);
  }
})

route.post('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const data = req.body;
    await setRating(id, data);
    res.send('Success');
  } catch (err) {
    log.error(err);
    res.status(HttpStatus.BAD_REQUEST).send(err);
  }
});

export default route;
