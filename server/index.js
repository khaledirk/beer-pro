import express from 'express';
import http from 'http';
import HttpStatus from 'http-status-codes';
import bodyParser from 'body-parser';
import _ from 'lodash';
import routes from './routes';
import { insertLog } from './lib/db_operations';

require('colors');

// Validate user included his email at the request header or return error
const validateRequestHeader = (req, res, next) => {
  const emailRegEx = new RegExp([
    /^([a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9])/,
    /(?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?/,
    /(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)+$/,
  ].map(r => r.source).join(''));
  const resHeaderUser = req.headers['x-user'];
  if (_.isNil(resHeaderUser) || !resHeaderUser.match(emailRegEx)) {
    const errorMessage = 'Missing or invalid x-user email value in the request header'
    log.error(errorMessage);
    res.status(HttpStatus.BAD_REQUEST)
      .send({
        code: HttpStatus.BAD_REQUEST,
        title: errorMessage,
      });
  } else {
    return next();
  }
}

module.exports = class ExpressService {
  constructor(listenPort) {
    this.listenPort = listenPort;
    // bind all internal functions to this class they can be used externally
    _.bindAll(this, ['start', 'stop']);
  }

  start() {
    // if express server already running then do nothing
    if (this.server) {
      return
    }
    log.info('Starting Express server....');

    this.app = express();
    this.http = http.Server(this.app);
    // Validate user email at request header
    this.app.use(validateRequestHeader);
    // Disable the ability to incorporate json object within the query string
    this.app.use(bodyParser.urlencoded({ extended: false }));
    // Allow up to 50 mega byte of body request
    this.app.use(bodyParser.json({ limit: '50mb', type: '*/*' }));
    // log any incoming request to console and to nosql db
    this.app.use((req, res, next) => {
      const user = req.headers['x-user'];
      insertLog(user, req.method, req.url, req.body);
      log.info(`${req.method} ${decodeURI(req.url)}`,
        `${JSON.stringify(req.body)}`.cyan);
      return next();
    });
    // All endpoint call to API will prefixed with /api to 
    // differenciate between API calls and html calls
    this.app.use('/api', routes);
    // Send 404 if url call do not fall under any route
    this.app.use((req, res) => {
      res.status(HttpStatus.NOT_FOUND)
        .send({ code: HttpStatus.NOT_FOUND, title: `Not found: ${req.url}` });
    });

    // Log error and send 500 if an error occured 
    this.app.use((err, req, res) => {
      log.error('Internal Server Error', err, err.stack);
      res.status(HttpStatus.INTERNAL_SERVER_ERROR)
        .send({
          code: HttpStatus.INTERNAL_SERVER_ERROR,
          title: 'Internal Server Error'
        });
    });

    log.info(`Starting server on port ${this.listenPort}`);
    this.server = this.http.listen(this.listenPort);
  }

  async stop() {
    if (this.server) {
      try {
        log.info('Stopping Express server');
        this.server.close(() => {
          log.info('--> Server Closing !!');
          process.exit();
        });
      } catch (err) {
        log.error('Error stopping express server', err);
      }
    }
  }

  // Exposing express for Jest unit testing
  app() {
    return this.app;
  }
}
